import React from "react";
import logo from "./logo.svg";
import "./App.css";

function App() {
  return (
    <div className="container">
      <h1 align="center">Form Pembelian Buah</h1>
      <form action="post">
        <div className="form-group">
          <label>Nama Pelanggan</label>
          <input type="text" name="nama" />
        </div>
        <div className="form-group">
          <label>Daftar Item</label>
          <div className="radio-box">
            <input type="checkbox" name="item"/>Semangka<br/>
            <input type="checkbox" name="item"/>Jeruk<br/>
            <input type="checkbox" name="item"/>Nanas<br/>
            <input type="checkbox" name="item"/>Salak<br/>
            <input type="checkbox" name="item"/>Anggur
          </div>
        </div>
        <button type="submit">Kirim</button>
      </form>
    </div>
  );
}

export default App;
